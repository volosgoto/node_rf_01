let sum = require("./sum");
let sub = require("./sub");
let mult = require("./mult");
let div = require("./div");

module.exports = {
    sum,
    sub,
    mult,
    div,
};
