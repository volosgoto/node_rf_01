let actionHandler = require("./actionHandler");

let [operation, ...numbers] = process.argv.slice(2);
let convertedToNumbersArr = numbers.map((item) => {
    return Number(item);
});

actionHandler(operation, convertedToNumbersArr);
