// console.log(process.argv);

// let args = process.argv.slice(2);
// console.log(args);

// sum +
// sub -
// mult *
// div /

let [operation, ...numbers] = process.argv.slice(2);

let convertedToNumbersArr = numbers.map((item) => {
    return Number(item);
});

// console.log(convertedToNumbersArr);

// console.log(operation);
// console.log(numbers);

// Number()
// parseFloat()
// parseInt()

// console.log(numbers);
// console.log(typeof numbers[0]);
// console.log(numbers[0]);

// if (operation === "sum") {
//     let result = convertedToNumbersArr.reduce((acc, num) => {
//         return acc + num;
//     }, 0);
//     console.log(result);
// }

// if (operation === "sub") {
//     let result = convertedToNumbersArr.reduce((acc, num) => {
//         return acc - num;
//     });
//     console.log(result);
// }

// if (operation === "mult") {
//     let result = convertedToNumbersArr.reduce((acc, num) => {
//         return acc * num;
//     });
//     console.log(result);
// }

// if (operation === "div") {
//     let result = convertedToNumbersArr.reduce((acc, num) => {
//         return acc / num;
//     });
//     console.log(result);
// }

let sum = (operationType, convertedToNumbersArr) => {
    if (operationType === "sum") {
        let result = convertedToNumbersArr.reduce((acc, num) => {
            return acc + num;
        }, 0);
        console.log(result);
        return true;
    }
    return null;
};

let sub = (operationType, convertedToNumbersArr) => {
    if (operationType === "sub") {
        let result = convertedToNumbersArr.reduce((acc, num) => {
            return acc - num;
        });
        console.log(result);
        return true;
    }
    return null;
};

let mult = (operationType, convertedToNumbersArr) => {
    if (operationType === "mult") {
        let result = convertedToNumbersArr.reduce((acc, num) => {
            return acc * num;
        });
        console.log(result);
        return true;
    }
    return null;
};

let div = (operationType, convertedToNumbersArr) => {
    if (operationType === "div") {
        let result = convertedToNumbersArr.reduce((acc, num) => {
            return acc / num;
        });
        console.log(result);
        return true;
    }
    return null;
};

switch (operation) {
    case "sum":
        sum(operation, convertedToNumbersArr);
        break;

    case "sub":
        sub(operation, convertedToNumbersArr);
        break;

    case "mult":
        mult(operation, convertedToNumbersArr);
        break;

    case "div":
        div(operation, convertedToNumbersArr);
        break;

    default:
        console.error("Unable to handle operation");
}
