// require('fs').promises
let fs = require("fs/promises");
let path = require("path");

console.log("join", path.join());
console.log("resolve", path.resolve());

let productPath = path.join(__dirname, "..", "db", "products.json");

// console.log(productPath);
// console.log(fs);
// console.log(__dirname);
// console.log(__filename);

let getAll = async () => {
    let data = await fs.readFile(productPath);
    let products = JSON.parse(data);
    // console.log(products);
    return products;
};

module.exports = getAll;
