let getAll = require("./getAll");
let getOne = require("./getOne");
let update = require("./update");
let remove = require("./remove");
let create = require("./create");

module.exports = {
    getAll,
    getOne,
    update,
    remove,
    create,
};
