let vova = "Vova";
let sara = "Sara";

let sayHello = (userName) => {
    console.log(`Hello ${userName}`);
};

let cart = [
    { id: 123, title: "Apple", price: 1200 },
    { id: 555, title: "Samsung", price: 800 },
];
//ES6
module.exports = {
    vova,
    sara,
    sayHello,
    cart,
};

// ''
// ""
// `` backticks
// `` - template string

// module.exports = vova

// module.exports = {
//     vova: vova,
//     sara: sara,
// };
