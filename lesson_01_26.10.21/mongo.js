// find all documents
await MyModel.find({});

// find all documents named john and at least 18
await MyModel.find({ name: "john", age: { $gte: 18 } }).exec();
